/**
 * =================================================================
 * FILE : Point.hpp
 * DESC : Algorithms and structs for discrete geometry in {2/3}D
 * AUTH : Thibault de VILLÈLE
 * DATE : 17th April 2019
 * =================================================================
 */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "Vector.hpp"

#define GRAHAM_SET_CENTER_AS_BARYCENTER

#define GRAHAM_FORWARD_CHECKING

#define EPSILON 1e-4;

namespace utils {
	/**
	 * Creates a random set of points uniformally in a sphere
	 * at a distance <radius> from the origin point <center>.
	 */
	std::vector<Point2> randomizedPoints(const Point2 center, double radius, unsigned int number);

	/**
	 * Returns true if T is on the right from the edge R-S
	 * ((one sided, edge always FROM R TO S))
	 */
	bool concave(const Point2 R, const Point2 S, const Point2 T);

	/**
	 * Returns true if T is on the right from the edge R-S
	 * ((one sided, edge always FROM R TO S))
	 */
	bool concaveWithVector(const vec2 RS, const Point2 R, const Point2 T);

	/**
	 * Represents an edge between 2 points in subsequent algorithms
	 */
	struct boundaryEdge {
		unsigned int start;
		unsigned int end;
		boundaryEdge* last;
		boundaryEdge* next;

		/**
		 * creates a boundary edge
		 */
		boundaryEdge(unsigned int, unsigned int);

		/**
		 * compares 2 boundary edges
		 */
		bool operator==(const boundaryEdge&) const;

		/**
		 * Extraction operator for printing to screen
		 */
		friend std::ostream& operator<<(std::ostream& out, const boundaryEdge b) {
			return out << b.start << " -> " << b.end << " // ";
		}
	};

	struct edge {
		unsigned int start;
		unsigned int end;

		/**
		 * create an edge
		 */
		edge(unsigned int, unsigned int);
	};

	void printToFile(const char* pathName, const std::vector<Point2> allPoints, boundaryEdge* boundary = nullptr, int nbEdges = 0);

	void printToFile(const char* pathName, const std::vector<Point2> allPoints, std::vector<edge> boundary = std::vector<edge>());

	int countEdges(boundaryEdge*);

	bool isVisible(Point2 R, Point2 S, Point2 T, Point2 origin = Point2(0.0,0.0));
}

typedef utils::boundaryEdge boundaryEdge;

typedef utils::edge edge;

typedef std::vector<edge> boundary_pairs;

namespace algorithms {
	/**
	 * DEPRECATED -- The pointers were fucking me up --DEPRECATED
	 * creates a convex envelope of the given point cloud in 2D.
	 * Returns a doubly linked list of boundary edges which define
	 * the convex envelope of the point cloud.
	 */
	boundaryEdge* grahamAlgorithm(const std::vector<Point2> pointCloud, double centerX = 512.0, double centerY = 512.0);

	/**
	 * creates a convex envelope of the given point cloud in 2D.
	 * Returns a vector of the edges created by the algorithm.
	 */
	boundary_pairs grahamAlgorithmSimpler(const std::vector<Point2> pointCloud, double centerX = 512.0, double centerY = 512.0);

	boundary_pairs visibilityConvexHullAlgorithm(const std::vector<Point2> pointCloud, double centerX = 512.0, double centerY = 512.0);
}

#endif // UTILS_HPP