#include "utils.hpp"

#include <utility>
#include <algorithm>

std::vector<Point2> utils::randomizedPoints(const Point2 center, double radius, unsigned int number) {
	std::vector<Point2> generatedPoints;

	// Seed from current time
	std::srand(std::time(nullptr));

	for (unsigned int i = 0; i < number; ++i) {
		// random distance in [0,radius]
		double distance = radius * ( (double)rand() / ((double)RAND_MAX+1) );
		// random angle
		double angle = 2.0 * M_PI * ( (double)rand() / ((double)RAND_MAX+1) );
		// position on the unit circle
		Point2 unitCirclePos = Point2(std::cos(angle), std::sin(angle));
		// scaled
		Point2 circlePos = unitCirclePos * distance;
		// translated
		Point2 finalPos = circlePos + center;
		// add to vector
		generatedPoints.push_back(finalPos);
	}

	return generatedPoints;
}

bool utils::concave(const Point2 R, const Point2 S, const Point2 T) {
	// Create edge from R to S :
	vec2 middle = S - R;
	// create edge from R to T:
	vec2 comparable = T - R;
	// get determinant in 2D
	double det = middle.x * comparable.y - middle.y * comparable.x;
	return det < 0.0;
}

bool utils::concaveWithVector(const vec2 RS, const Point2 R, const Point2 T) {
	// Create edge from R to S :
	// create edge from R to T:
	vec2 comparable = T - R;
	// get determinant in 2D
	double det = RS.x * comparable.y - RS.y * comparable.x;
	return det < 0.0;
}

void utils::printToFile(const char* pathName, const std::vector<Point2> allPoints, boundaryEdge* boundary, int nbes) {
	std::ofstream out(pathName, std::ios::out);
	out << "ply" << std::endl;
	out << "format ascii 1.0" << std::endl;
	out << "element vertex " << allPoints.size() << std::endl;
	out << "property float x" << std::endl;
	out << "property float y" << std::endl;
	out << "property float z" << std::endl;
	if (boundary != nullptr) {
		int nbEdges = 0;
		for (boundaryEdge* currentEdge = boundary; currentEdge->next != boundary; currentEdge = currentEdge->next) {
			nbEdges++;
		}
		nbEdges++;
		out << "element edge " << nbEdges << std::endl;
		out << "property int vertex1" << std::endl;
		out << "property int vertex2" << std::endl;
	}
	out << "end_header" << std::endl;
	for (Point2 p : allPoints) {
		out << (float)p.x << " " << (float)p.y << " " << "0.0" << std::endl;
	}
	if (boundary != nullptr) {
		for (boundaryEdge* currentEdge = boundary; currentEdge->next != boundary; currentEdge = currentEdge->next) {
			out << currentEdge->start << " " << currentEdge->end << std::endl;
		}
		out << boundary->last->start << " " << boundary->last->end << std::endl;
		// for (int i = 0; i <= nbEdges; ++i) {
		// 	out << boundary->start << " " << boundary->end << std::endl;
		// 	if (boundary->next != nullptr) {
		// 		boundary = boundary->next;
		// 	}
		// }
	}
	out.close();
	return;
}

utils::boundaryEdge::boundaryEdge(unsigned int s, unsigned int e) {
	this->start = s;
	this->end = e;
	this->last = nullptr;
	this->next = nullptr;
}

utils::edge::edge(unsigned int a, unsigned int b) {
	this->start = a;
	this->end = b;
}

bool utils::boundaryEdge::operator==(const utils::boundaryEdge& other) const {
	return this->start == other.start && this->end == other.end;
}

bool utils::isVisible(Point2 R, Point2 S, Point2 T, Point2 origin) {
	/**
	 * We make the assumption the edge goes 
	 * from R to S, needs to be visible 
	 * from T, and is computed from origin.
	 */
	bool directionFromOrigin = utils::concave(origin, R, S);
	bool directionFromT = utils::concave(T,S,R);

	return directionFromOrigin && !directionFromT;
}

utils::boundaryEdge* algorithms::grahamAlgorithm(const std::vector<Point2> pointCloud, double centerX, double centerY) {
	/**
	 * start by computing barycenter
	 */
	#ifdef GRAHAM_SET_CENTER_AS_BARYCENTER
		Point2 barycenter(centerX,centerY);
		std::cout << "Warning : making the assumption the center of the 2D space is the barycenter of the point cloud." << std::endl;
		std::cout << "\tThe center was deemed to be at (" << centerX << "," << centerY << ")." << std::endl;
	#else
		Point2 barycenter(0.0,0.0);
		for (unsigned int i = 0 ; i < pointCloud.size(); ++i) {
			barycenter += pointCloud[i];
		}
		barycenter /= (double)pointCloud.size();
	#endif

	std::vector< std::pair<double, unsigned int> > anglePointPair;
	
	/**
	 * then, compute all angles from barycenter
	 */
	for (unsigned int i = 0; i < pointCloud.size(); ++i) {
		double angle = atan2(pointCloud[i].y - barycenter.y, pointCloud[i].x - barycenter.x);
		anglePointPair.push_back(std::make_pair(angle,i));
	}
	
	/**
	 * sort the indices
	 */
	std::sort(anglePointPair.begin(), anglePointPair.end(), [](std::pair<double,unsigned int>lVal, std::pair<double,unsigned int>rVal) {
		return lVal.first < rVal.first;
	});

	/**
	 * create edges in between every one of them
	 */
	int currentIndex, nextIndex;
	boundaryEdge* lastBound = nullptr;
	boundaryEdge* firstEdge = nullptr;
	boundaryEdge* currentEdge;
	for (unsigned int i = 0; i < anglePointPair.size(); ++i) {
		currentIndex = anglePointPair[i].second;
		nextIndex = (i == anglePointPair.size()-1) ? anglePointPair[0].second : anglePointPair[i+1].second;
		currentEdge = new boundaryEdge(currentIndex, nextIndex);
		currentEdge->last = lastBound;
		if (lastBound != nullptr) {
			lastBound->next = currentEdge;
		}
		lastBound = currentEdge;
		if (i == 0) {firstEdge = currentEdge;}
	}
	currentEdge->next = firstEdge;
	firstEdge->last = currentEdge;

	/**
	 * compare them 2 by two
	 */
	boundaryEdge* stoppingCriteria = firstEdge;
	currentEdge = firstEdge;
	while (currentEdge != stoppingCriteria) {
		Point2 R = pointCloud[currentEdge->start];
		Point2 S = pointCloud[currentEdge->end];
		Point2 T = pointCloud[currentEdge->next->end];
		if (utils::concave(R,S,T)) {
			if (currentEdge->next == stoppingCriteria) {
				stoppingCriteria = stoppingCriteria->next;
			}
			/**
			 * "fast-forward" the currentEdge to next index
			 */
			currentEdge->end = currentEdge->next->end;
			currentEdge->next = currentEdge->next->next;
			currentEdge->next->next->last = currentEdge;
			if (currentEdge->last != stoppingCriteria) {
				currentEdge = currentEdge->last;
			}
		} else {
			currentEdge = currentEdge->next;
		}
	}

	std::cout << "Finished graham" << std::endl;

	return firstEdge;
}

boundary_pairs algorithms::grahamAlgorithmSimpler(const std::vector<Point2> pointCloud, double centerX, double centerY) {
	/**
	 * start by computing barycenter
	 */
	#ifdef GRAHAM_SET_CENTER_AS_BARYCENTER
		Point2 barycenter(centerX,centerY);
		std::cout << "Warning : making the assumption the center of the 2D space is the barycenter of the point cloud." << std::endl;
		std::cout << "\tThe center was deemed to be at (" << centerX << "," << centerY << ")." << std::endl;
	#else
		Point2 barycenter(0.0,0.0);
		for (unsigned int i = 0 ; i < pointCloud.size(); ++i) {
			barycenter += pointCloud[i];
		}
		barycenter /= (double)pointCloud.size();
	#endif

	std::vector< std::pair<double, unsigned int> > anglePointPair;
	boundary_pairs edges;
	
	/**
	 * then, compute all angles from barycenter
	 */
	for (unsigned int i = 0; i < pointCloud.size(); ++i) {
		double angle = atan2(pointCloud[i].y - barycenter.y, pointCloud[i].x - barycenter.x);
		anglePointPair.push_back(std::make_pair(angle,i));
	}
	
	/**
	 * sort the indices
	 */
	std::sort(anglePointPair.begin(), anglePointPair.end(), [](std::pair<double,unsigned int>lVal, std::pair<double,unsigned int>rVal) {
		return lVal.first < rVal.first;
	});

	/**
	 * create edges in between every one of them
	 */
	int currentIndex, nextIndex;
	for (unsigned int i = 0; i < anglePointPair.size(); ++i) {
		currentIndex = anglePointPair[i].second;
		nextIndex = (i == anglePointPair.size()-1) ? anglePointPair[0].second : anglePointPair[i+1].second;
		edges.push_back(edge(currentIndex, nextIndex));
	}

	/**
	 * compare them 2 by two
	 */
	int i = 0;
	while (i < (int)edges.size()) {
		Point2 R = pointCloud[edges[i].start];
		Point2 S = pointCloud[edges[i].end];
		nextIndex = (i == (int)edges.size()-1) ? 0 : i+1;
		Point2 T = pointCloud[edges[nextIndex].end];
		if (utils::concave(R,S,T)) {
			/**
			 * "fast-forward" the currentEdge to next index
			 */
			edges[i].end = edges[nextIndex].end;
			if (i < edges.size()-1) {
				edges.erase(edges.begin()+i+1);
			}
			i = (i == 0) ? 0 : i-1;
		} else {
			i++;
		}
	}

	std::cout << "Finished graham" << std::endl;

	return edges;
}

void utils::printToFile(const char* pathName, const std::vector<Point2> allPoints, boundary_pairs boundary) {
	int nbEdges = boundary.size();
	std::ofstream out(pathName, std::ios::out);
	out << "ply" << std::endl;
	out << "format ascii 1.0" << std::endl;
	out << "element vertex " << allPoints.size() << std::endl;
	out << "property float x" << std::endl;
	out << "property float y" << std::endl;
	out << "property float z" << std::endl;
	if (nbEdges != 0) {
		out << "element edge " << nbEdges << std::endl;
		out << "property int vertex1" << std::endl;
		out << "property int vertex2" << std::endl;
	}
	out << "end_header" << std::endl;
	for (Point2 p : allPoints) {
		out << (float)p.x << " " << (float)p.y << " " << "0.0" << std::endl;
	}
	if (nbEdges != 0) {
		for (edge e : boundary) {
			out << e.start << " " << e.end << std::endl;
		}
	}
	out.close();
	return;
}

int utils::countEdges(boundaryEdge* start) {
	std::cout << "Counting edges ... " << std::endl;
	boundaryEdge* endCond = start->last;
	if (start->next == nullptr) {
		return 1;
	}
	int nbEdges = 1;
	for (boundaryEdge* i = start; i->next != nullptr; i = i->next) {
		nbEdges++;
		if (i == endCond) {
			break;
		}
	}
	std::cout << "Counted edges" << std::endl;
	return nbEdges;
}

boundary_pairs algorithms::visibilityConvexHullAlgorithm(std::vector<Point2> pointCloud, double centerX, double centerY) {
	// Sort all points by X then Y :
	std::sort(pointCloud.begin(), pointCloud.end(), [](Point2 lVal, Point2 rVal){
		if (lVal.x == rVal.x) {
			return lVal.y < rVal.y;
		}
		return lVal.x < rVal.x;
	});

	boundary_pairs boundary;

	// Get a first polygon by the 3 first points
	boundary.push_back(edge(0,1));
	boundary.push_back(edge(1,2));
	boundary.push_back(edge(2,0));

	for (int i = 3; i < pointCloud.size(); ++i) {
		Point2 toAdd = pointCloud[i];
		for (int j = 0; j < boundary.size(); j++) {
			Point2 edgeOrigin = pointCloud[boundary[j].start];
			Point2 edgeEnd = pointCloud[boundary[j].end];
			// if the edge's visible and not completely disconnected from the first visible edge
			int lastVisible = -1;
			if (utils::isVisible(edgeOrigin, edgeEnd, toAdd) && (j == lastVisible-1 && lastVisible >= 0)) {
				unsigned int oldEnd = boundary[j].end;
				boundary[j].end = i;
				boundary.emplace(boundary.begin()+j+1, edge(i, oldEnd));
				lastVisible = j;
			}
		}
	}

	return boundary;
}