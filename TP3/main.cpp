#include <iostream>

#include "Vector.hpp"
#include "utils.hpp"

#include <vector>
#include <cstring>

int main(int argc, char* argv[]) {
	/**
	 * Lazy argument parsing
	 */
	switch (argc) {
		case 2:
			if (strcmp(argv[1], "--usage") == 0) {
				std::cout << "Usage : " << std::endl;
				std::cout << "\t" << argv[0] << std::endl;
				return EXIT_SUCCESS;
			}
			break;
		default:
			break;
	};


	// Max x boundary
	double xMax = 20.0;
	// Max y boundary
	double yMax = 20.0;
	// Center point
	Point2 center(xMax/2.0, yMax/2.0);
	// get random points : 
	std::vector<Point2> allPoints = utils::randomizedPoints(center, xMax/2.0-1.0, 10);
	// Get convex envelope :
	boundary_pairs edges = algorithms::grahamAlgorithmSimpler(allPoints, xMax/2.0, yMax/2.0);
	utils::printToFile("result.ply", allPoints, edges);
	return EXIT_SUCCESS;
}