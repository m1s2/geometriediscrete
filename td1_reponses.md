### Exercice 1 :

1. 

2. Les tirets représentent la distance minimale pour arriver à ce point
  a. La inégalité triangulaire
  b. Il faut remplacer les tirets par 0 dans le premier masque, et par 14 pour le deuxième masque

3. algo, cf le tp. complexité : 

4. pour 2x2, le meilleur est 8/11, qui donne une erreur de ~=~ 0.036

5. a < b < c, et c < b+a sinon c ne sert à rien

input : image I, image II, masque M, offset d'appel de n, position d'appel (nx,ny), distance max D
output : image I transformée
chamfreinSource(I,II,M,nx,ny,D):
	m_c_x <-- centre.x masque
	m_c_y <-- centre.y masque
	max_x_ <-- (largeur masque - 1) / 2
	max_y_ <-- (hauteur masque - 1) / 2
	p_x <-- (largeur_image) / 2 + nx
	p_y <-- (hauteur_image) / 2 + ny

	pour m_x allant de 0 à max_x_ :
		pour m_y allant de 0 à m_x :
			si 
				masque[m_c_x+m_x][m_c_y+m_y] + n 
				<= 
				II[p_x+m_x][p_y+m_y] 
				ET si 
					masque[m_c_x+m_x][m_c_y+m_y] 
					+ II[p_x+m_x][p_y+m_y] 
					+ n <= D
				affecter masque[m_c_x+m_x][m_c_y+m_y] + n à :
					- II[p_x+m_x][p_y+m_y]
					- II[p_x+m_x][p_y-m_y]
					- II[p_x-m_x][p_y-m_y]
					- II[p_x-m_x][p_y+m_y]
					- II[p_x+m_y][p_y+m_x]
					- II[p_x+m_y][p_y-m_x]
					- II[p_x-m_y][p_y-m_x]
					- II[p_x-m_y][p_y+m_x]
				appel à chamfrein(I, II, M, masque[m_c_x+m_x][m_c_y+m_y] + n, p_x+m_x, p_y+m_y D);
				colorier à 1 / imageII.valeur les points
			sinon
				ne pas colorier les points
			finsi
		fin pour tout
	fin pour tout
	I <- II
fin