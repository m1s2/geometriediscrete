# HMIN235 - Notes de cours

### Notion de distance

$$ d_2 (x,y) = \sqrt{\mid x_1 - y_1 \mid ^2 + \mid x_2 - y_2 \mid ^2} $$

### Axiomes des distances

  - positive : $\forall q \in E, 0 \leq d(p,q)$
  - definie : $\forall p,q \in E, d(p,q) = 0 <=> p = q$
  - symétrique : $\forall p,q \in E, d(p,q) = d(q,p)$
  - triangulaire : $\forall p,q,r \in E, d(p,q) \leq d(p,r) + d(p,q)$

### Distance de haussdorf (distance entre deux ensembles de points)

Soient deux ensembles X et Y :

$$ d(p, X) = min( d(p,q) : q \in X ) $$

$$ Hd(X,Y) = max \mid{max\mid{d(p,Y) : p \in X}\mid , max\mid{d(q,X) : q \in Y}\mid}\mid $$

### Les normes

Une norme est définie sur un vecteur, qui nécessite un espace vectoriel, qui nécessite un corps.

### Modules

La norme sur un module contient les axiomes suivants :

  - positive : $\forall x \in E, g(x) \geq 0$
  - définie : $\forall x \in E, g(x) = 0 \Leftrightarrow x = 0$
  - triangulaire : $\forall x,y \in E, g(x+y) \leq g(x) + g(y)$
  - homogène : $\forall x \in E, \lambda \in K, g(\lambda \times x) = \mid \lambda \mid \times g(x)$

Si on considère $g : E \rightarrow F$ une norme sur $K$, dans son application on peut définir deux autres axiomes :

  - inveriance par translation : $\forall p,q,r \in E, d(p+r, q+r) = d(p,q)$
  - homogénéité : $\forall p,q \in E, \lambda \in K, d(\lambda \times p, \lambda \times q) = \mid\lambda\mid \times d(p,q)$