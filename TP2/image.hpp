#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <iostream>
#include <vector>
#include <string>

class droite;

class image {
public:
	/**
	 * image matrix for the pixels
	 */
	std::vector<std::vector<unsigned char>> imageMatrix;

	/**
	 * width and height parameters for the picture
	 */
	int width, height;

	/**
	 * Basic constructor
	 */
	image();

	/**
	 * constructor specifying a height and width for the image
	 */
	image(const int h, const int w);

	/**
	 * copy constructor
	 */
	image(const image&);

	/**
	 * apply a straight line to a file, optionnaly at indexes X Y (ordered like a math grid, with X and Y at the bottom left)
	 */
	void applyStraightLine(const droite&);

	/**
	 * write file to disk, at the path specified.
	 */
	void writeToFile(const std::string& pathname) const;
};

#endif