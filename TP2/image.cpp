#include "image.hpp"
#include "droite.hpp"

#include <fcntl.h>

image::image() {
	this->height = 0;
	this->width = 0;
	this->imageMatrix = {std::vector<unsigned char>()};
}

image::image(const int h, const int w) {
	this->height = h;
	this->width = w;

	this->imageMatrix.resize(this->height);
	for (int i = 0; i < this->height; ++i) {
		this->imageMatrix[i].resize(this->width);
		for (int j = 0; j < (int) this->imageMatrix[i].size(); ++j) {
			this->imageMatrix[i][j] = 0;
		}
	}
}

void image::applyStraightLine(const droite& d) {
	if (d._heightMap.size() == 0) {
		return;
	}
	int x , y ;

	for (int i = 0; i < (int)d._heightMap.size(); ++i) {
		if (i >= 0 && i < this->width) {
			x = d._heightMap[i].first;
			y = d._heightMap[i].second;
			if (y < this->height && y >= 0) {
				this->imageMatrix[this->height-y+1][x] = 255;
			}
		}
	}

	return;
}

void image::writeToFile(const std::string& pathName) const {
	if (this->height == 0 || this->width == 0) {
		std::cerr << "couldn't write an image of width " << this->width << " and height " << this->height << " onto disk : TOO SMALL" << std::endl;
		return;
	}

	FILE* imageFile = fopen(pathName.c_str(), "wb");

	// Write image type
	fprintf(imageFile, "P5\n");
	// Write comment
	fprintf(imageFile, "# Image was generated from a line equation\n");
	// Write image size and max vals
	fprintf(imageFile, "%d %d\n%d\n", this->width, this->height, 255);

	// Convert matrix to array
	int imgSize = this->width * this->height;
	unsigned char* rawData = new unsigned char[imgSize];
	for (unsigned int y = 0; y < this->imageMatrix.size(); ++y) {
		for (unsigned int x = 0; x < this->imageMatrix[y].size(); ++x) {
			rawData[y*width + x] = imageMatrix[y][x];
		}
	}

	if (fwrite(rawData, sizeof(unsigned char), imgSize, imageFile) != (size_t)imgSize) {
		std::cerr << "Error whilst printing to file " << pathName << ". Exiting ..." <<std::endl;
		return;
	}

	fclose(imageFile);

	return;
}
