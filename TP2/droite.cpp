#include "droite.hpp"

#include <iostream>
#include <limits>

droite::droite() {
	this->_x0 = std::numeric_limits<unsigned char>::max();
	this->_x1 = std::numeric_limits<unsigned char>::min();
	this->_y0 = std::numeric_limits<unsigned char>::max();
	this->_y1 = std::numeric_limits<unsigned char>::min();
}

droite::droite(const int x0, const int y0, const int x1, const int y1, bool algo) {
	this->_x0 = x0;
	this->_x1 = x1;
	this->_y0 = y0;
	this->_y1 = y1;
	if (algo) {
		this->computeWithBrensenham();
	} else {
		this->computeWithReveille();
	}
}

droite::droite(const droite& d) {
	this->_x0 = d._x0;
	this->_x1 = d._x1;
	this->_y0 = d._y0;
	this->_y1 = d._y1;
	this->_heightMap = d._heightMap;
}

void droite::computeWithBrensenham() {
	int dx = _x1 - _x0;
	int dy = _y1 - _y0;

	int incrHor = 2 * dy;
	int incrDiag = 2*(dx - dy);

	int e = 2*dy - dx;

	// indices of while loop
	int x = _x0;
	int y = _y0;

	while (x < _x1) {
		this->_heightMap.push_back(std::make_pair(x,y));
		if (e > 0) {
			y++;
			e -= incrDiag;
		} else {
			e += incrHor;
		}
		x++;
	}
}

void droite::computeWithReveille() {
	int vx = _x1 - _x0;
	int vy = _y1 - _y0;

	int mu = vy * _x0 - vx * _y0;
	int r  = vy * _x0 - vx * _y0 - mu;

	int x = _x0;
	int y = _y0;

	do {
		x++;
		r += vy;
		if (r < 0 || r >= vx) {
			y++;
			r-=vx;
		}
		this->_heightMap.push_back(std::make_pair(x,y));
	} while (x < _x1);
}