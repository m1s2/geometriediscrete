#ifndef DROITE_HPP
#define DROITE_HPP

#include <vector>
#include <utility>

class droite {
public:
	/**
	 * pair values of all points of the line
	 */
	std::vector<std::pair<int,int>> _heightMap;
	int _x0, _y0, _x1, _y1;

	/**
	 * basic constructor
	 */
	droite();

	/**
	 * constructor with a line to trace
	 */
	droite(const int x0, const int y0, const int x1, const int y1, bool algo = true);

	/**
	 * copy constructor
	 */
	droite(const droite&);

	/**
	 * compute line with brensenham algorithm
	 */
	void computeWithBrensenham();

	/**
	 * compute line with reveillés algorithm
	 */
	void computeWithReveille();
};

#endif