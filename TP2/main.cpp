#include <iostream>

#include "image.hpp"
#include "droite.hpp"

int main(int argc, char const *argv[]) {
	if (argc != 6) {
		std::cout << "Usage : \n\t" << argv[0] << " ./fileOut.pgm x0 y0 x1 y1" << std::endl;
		return EXIT_FAILURE;
	}
	image i(200, 200);
	droite d(atoi(argv[2]),atoi(argv[3]),atoi(argv[4]),atoi(argv[5]), 1);
	i.applyStraightLine(d);
	i.writeToFile(argv[1]);
	return 0;
}
