/* This may look like C code, but it is really -*- C -*-
 
	Copyright (C) 2000 LIRMM

	Author: Christophe Fiorio <fiorio@lirmm.fr>
	This is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2, or (at your option)
	any later version.
 
	This sofware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
 
	You may have received a copy of the GNU General Public License along
	with GNU Emacs; see the file COPYING.  If not, write to the Free
	Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 
	@(#) $Id: pgm_basic_io.h,v 1.1 2000/03/21 19:12:20 fiorio Exp $
 
	$Log: pgm_basic_io.h,v $
	Revision 1.1  2000/03/21 19:12:20  fiorio
	Initial revision


*/

#ifndef pgm_basic_io_h
#define pgm_basic_io_h

#include <stdio.h>

/**
 * image matrix, containing width and height info
 */
typedef struct image {
	unsigned char** pixels;
	int w, h;
} image2D;

/**
 * Mask struct to hold the values
 */
typedef struct mask {
	char** values;
	int w;
} mask2D;

/**
 * XY coordinates in the image
 */
typedef struct coordinates {
	int x,y;
} pair;

/**
 * Stack of XY coordinates
 */
typedef struct stackToAnalyse {
	int* vals;
	int top;
} stack;

/**
 * allocates the stack
 */
void initStack(stack*);

/**
 * return the top stack pair
 */
int* pop(stack*);

/**
 * push a pair to the stack
 */
void push(stack*, int, int);

/**
 * checks the stack size
 */
int isStackEmpty(stack*s);

/**
 * checks an item is in stack or not
 */
int isInStack(stack*, int x, int y);

/**
 * Fonction qui transforme une image 1D en 2D. Pour plus de libertés
 */
image2D convertTo2D(unsigned char* image, int w, int h);

image2D createImage(int w, int h);

image2D copyImage(image2D*);

image2D createWhiteImage(int w, int h);

void symmetry8(image2D* i);
/**
 * Converts a image2D struct into an image to write
 */
unsigned char* convertTo1D(image2D image);

/**
 * Apply the chamfer algorithm to the image.
 */
void chamfer(image2D* imgIn, image2D* io, unsigned char maxDist);

/**
 * Recursive function calling the chamfer algorithm on a sub-image of the mask
 */
void chamferRecursive(image2D* imgIn, image2D* imgOut, mask2D* mask, int distOff, int cPX, int cPY, int maxDist);

/**
 * iterative chamfer function
 */
void chamferIterative(image2D* in, image2D* out, mask2D* mask, unsigned char maxDist);

/**
 * return lowest value
 */
int min(int, int);

/**
 * computes the skeleton of the image
 */
void distanceImage(image2D* in, image2D* out, mask2D* m);

/*-----------------------------------------------------------------------------------*/


/* --------------------------------------------------------------- 
 *  Function litPGM : lit une image au format PGM P5 et la stocke
 *                     dans une matrice d'unsigned char. R�alise
 *                     l'allocation m�moire
 *
 *  @param FILE* : le fichier contenant l'image (�ventuellement stdin)
 *  @return int* : le nombre de colonnes (la largeur)
 *  @return int* : le nombre de lignes (la hauteur)
 *  @return unsigned char* : la matrice contenant l'image
 * --------------------------------------------------------------- */
unsigned char* litPGM(FILE*, int*, int*);


/* --------------------------------------------------------------- 
 *  Function ecritPGM : 
 *
 *  @param FILE* : le fichier dans lequel on �crit (�ventuellement stdout)
 *  @param unsigned char*: la matrice contenant l'image
 *  @param int  : la largeur (nombre de colonnes)
 *  @param int  : la hauteur (nombre de lignes)
 *  @return FILE* : NULL si erreur, le fichier dans lequel on a �crit.
 * --------------------------------------------------------------- */
FILE* ecritPGM(FILE*, unsigned char*, int, int);


/*-----------------------------------------------------------------------------------*/

#endif
