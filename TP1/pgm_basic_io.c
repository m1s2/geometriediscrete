/* This may look like C code, but it is really -*- C -*-

	Copyright (C) 1998 LIRMM

	Author: Christophe Fiorio <fiorio@lirmm.fr>
	This is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2, or (at your option)
	any later version.

	This sofware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You may have received a copy of the GNU General Public License along
	with GNU Emacs; see the file COPYING.  If not, write to the Free
	Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 
	@(#) $Id: pgm_basic_io.c,v 1.1 2000/03/21 19:12:20 fiorio Exp $

	$Log: pgm_basic_io.c,v $
	Revision 1.1  2000/03/21 19:12:20  fiorio
	Initial revision


*/

#ifndef pgm_basic_io_c
#define pgm_basic_io_c

#include "pgm_basic_io.h"

#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

// #define DEBUG
#define MAX_OUTPUT
// #define ANIMATE_RESULTS

void initStack(stack* s) {
	(*s).vals = (int*)malloc( USHRT_MAX * 2 * sizeof(int));
	(*s).top = 0;
	return;
}

int* pop(stack* s) {
	if ((*s).top == 0) {
		int* i = (int*)malloc(2*sizeof(int));
		i[0] = -1;
		i[1] = -1;
		return i;
	}
	int x = (*s).vals[2*(*s).top-2];
	int y = (*s).vals[2*(*s).top-1];

	int* i = (int*)malloc(2*sizeof(int));
	i[0] = x;
	i[1] = y;
	#ifdef DEBUG
	printf("popping %d %d\n", x,y);
	#endif

	(*s).top--;
	return i;
}

void push(stack* s, int x, int y) {
	if ((*s).top == LONG_MAX-1) {
		printf("reached top of stack. pixel %d %d lost\n", x,y);
		return;
	}
	#ifdef DEBUG
	printf("pushing to %d\n", (*s).top);
	#endif
	(*s).vals[2*(*s).top+0] = x;
	(*s).vals[2*(*s).top+1] = y;
	(*s).top++;
	return;
}

int isStackEmpty(stack* s) {
	if ((*s).top == 0) {
		return 1;
	}
	return 0;
}

int isInStack(stack* s, int x, int y) {
	for (int i = (*s).top; i > 0; --i) {
		if ((*s).vals[2*i-2] == x && (*s).vals[2*i-1] == y) {
			return 1;
		}
	}
	return 0;
}

image2D convertTo2D(unsigned char* image, int w, int h) {
	image2D imgFull;
	// create an array of pixel lines
	imgFull.pixels = (unsigned char**) malloc(h * sizeof(unsigned char*));
	// fill in each line
	for (int i = 0; i < h; ++i) {
		imgFull.pixels[i] = (unsigned char*) malloc(w * sizeof(unsigned char));
	}

	for (int y = 0; y < h; ++y) {
		for (int x = 0; x < w; ++x) {
			imgFull.pixels[y][x] = image[y*w + x];
		}
	}

	imgFull.w = w;
	imgFull.h = h;

	printf("Generated image of size %ix%i\n", imgFull.w, imgFull.h);

	return imgFull;
}

image2D createImage(int w, int h) {
	image2D img;
	img.pixels = (unsigned char**) malloc(h*sizeof(unsigned char*));
	for(int i = 0; i < h; ++i) {
		img.pixels[i] = (unsigned char*) malloc(w*sizeof(unsigned char));
	}
	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w; ++j) 
			img.pixels[i][j] = 0;

	img.w = w;
	img.h = h;

	return img;
}

image2D createWhiteImage(int w, int h) {
	image2D img;
	img.pixels = (unsigned char**) malloc(h*sizeof(unsigned char*));
	for(int i = 0; i < h; ++i) {
		img.pixels[i] = (unsigned char*) malloc(w*sizeof(unsigned char));
	}
	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w; ++j) 
			img.pixels[i][j] = 255;

	img.w = w;
	img.h = h;

	printf("created white image\n");

	return img;
}

void symmetry8(image2D* img) {
	int centerX = (*img).w / 2;
	int centerY = (*img).h / 2;

	for (int i = 0; i < (*img).h-centerX; ++i) {
		for (int j = 0; j <= i; ++j) {
			(*img).pixels[centerY + j][centerX - i] = (*img).pixels[centerY + j][centerX + i];
			(*img).pixels[centerY - j][centerX - i] = (*img).pixels[centerY + j][centerX + i];
			(*img).pixels[centerY - j][centerX + i] = (*img).pixels[centerY + j][centerX + i];
			(*img).pixels[centerY + i][centerX + j] = (*img).pixels[centerY + j][centerX + i];
			(*img).pixels[centerY + i][centerX - j] = (*img).pixels[centerY + j][centerX + i];
			(*img).pixels[centerY - i][centerX - j] = (*img).pixels[centerY + j][centerX + i];
			(*img).pixels[centerY - i][centerX + j] = (*img).pixels[centerY + j][centerX + i];
			// (*img).pixels[centerY + j][centerX + i] = 128;
		}
	}

	// FILE* f = fopen("file.pgm", "w");

	// if (f == NULL) {
	// 	printf("couldnt write\n");
	// 	exit(-1);
	// } else {
	// 	ecritPGM(f, convertTo1D((*img)), (*img).w, (*img).h);
	// }

	// fclose(f);

	return;
}

unsigned char* convertTo1D(image2D image) {
	unsigned char* img = (unsigned char*) malloc(image.w * image.h * sizeof(char));

	for (int y = 0; y < image.h; ++y) {
		for (int x = 0; x < image.w; ++x) {
			img[y*image.w + x] = image.pixels[y][x];
		}
	}

	return img;
}

void chamfer(image2D* imgIn, image2D* io, unsigned char maxDist) {
	(*io).pixels = (unsigned char**) malloc((*imgIn).h * sizeof(unsigned char*));
	for (int i = 0; i < (*imgIn).h; ++i) {
		(*io).pixels[i] = (unsigned char*) malloc((*imgIn).w*sizeof(unsigned char));
	}

	(*io).h = (*imgIn).h;
	(*io).w = (*imgIn).w;

	for (int i = 0; i < (*io).h; ++i) {
		for (int j = 0; j < (*io).w; ++j) {
			(*io).pixels[i][j] = 0;
		}
	}

	/**
	 * define the mask : 
	 */
	mask2D newmask;
	newmask.w = 5;
	// newmask.w = 3;
	char** mask = (char**) malloc( newmask.w * sizeof(char*));
	for (int i = 0; i < newmask.w; ++i) {
		mask[i] = (char*) malloc( newmask.w * sizeof(char));
	}
	
	mask[0][0] = -1; mask[0][1] = 11; mask[0][2] = -1; mask[0][3] = 11; mask[0][4] = -1;
	mask[1][0] = 11; mask[1][1] =  7; mask[1][2] =  5; mask[1][3] =  7; mask[1][4] = 11;
	mask[2][0] = -1; mask[2][1] =  5; mask[2][2] =  0; mask[2][3] =  5; mask[2][4] = -1;
	mask[3][0] = 11; mask[3][1] =  7; mask[3][2] =  5; mask[3][3] =  7; mask[3][4] = 11;
	mask[4][0] = -1; mask[4][1] = 11; mask[4][2] = -1; mask[4][3] = 11; mask[4][4] = -1;
	
	newmask.values = mask;

	int imgCenterX = ((*imgIn).w % 2 == 1) ? ((*imgIn).w-1)/2 : ((*imgIn).w)/2;
	int imgCenterY = ((*imgIn).h % 2 == 1) ? ((*imgIn).h-1)/2 : ((*imgIn).h)/2;

	// chamferRecursive(imgIn, io, &newmask, 0, imgCenterX, imgCenterY, maxDist);

	chamferIterative(imgIn, io, &newmask, maxDist);

	return;
}

void chamferRecursive(image2D* imgIn, image2D* imgOut, mask2D* mask, int distOff, int px, int py, int maxDist) {
	int maskCenterX = ((*mask).w-1)/2;
	int maskCenterY = maskCenterX;

	/**
	 * Stopping condition
	 */
	if (px == (*imgIn).w || py == (*imgIn).h) {
		return;
	}

	for (int x = 1; x < (*mask).w-maskCenterX && px + x < (*imgIn).w; ++x) {
		for (int y = 0; y <= x && py+y < (*imgIn).h; ++y) {
			/**
			 * if the value of mask + distance of the pixel < imgOut.value 
			 * AND
			 * if it is still smaller than the distance asked for
			 */
			if (	(*mask).values[maskCenterY+y][maskCenterX+x] != -1 &&
				( (*mask).values[maskCenterY+y][maskCenterX+x] + distOff <= (*imgOut).pixels[py+y][px+x] || (*imgOut).pixels[py+y][px+x] == 0 ) &&
				(*imgOut).pixels[py+y][px+x] + (*mask).values[maskCenterY+y][maskCenterX+x] + distOff <= maxDist ) {
				/**
				 * update the value of the pixel :
				 */
				//printf("Found a pixel to upadte a coordinates : %i %i from %i to %i \n", px+x, py+y, (*imgOut).pixels[py+y][px+x], (*mask).values[maskCenterY+y][maskCenterX+x] + distOff);
				unsigned char val = ((*mask).values[maskCenterY+y][maskCenterX+x] + distOff >= 255) ? 255 : (*mask).values[maskCenterY+y][maskCenterX+x] + distOff;
				(*imgOut).pixels[py+y][px+x] = val;
				chamferRecursive(imgIn, imgOut, mask, (*mask).values[maskCenterY+y][maskCenterX+x] + distOff, px+x, py+y, maxDist);
			}
		}
	}
}

/* --------------------------------------------------------------- 
 *  Function litPGM : lit une image au format PGM P5 et la stocke
 *                    dans une matrice d'unsigned char. Réalise
 *                    l'allocation mémoire
 *
 *  @param FILE* : le fichier contenant l'image (éventuellement stdin)
 *  @return  int* : le nombre de colonnes (la largeur)
 *  @return  int* : le nombre de lignes (la hauteur)
 *  @return unsigned char* : la matrice contenant l'image
 * --------------------------------------------------------------- */
unsigned char* litPGM(FILE* f, int* w, int* h)
{
	char ch[255]; /* chaine tampon pour la lecture dans l'image */
	int c;        /* caractère pour lire la valeur des pixels  */
	int i=0;      /* i compteur de boucle */
	int s=0;      /* s taille de l'image  */
	unsigned char* ret = NULL;
		
	fgets(ch,255,f); /* le format */
	if( strncmp(ch,"P5",2)!=0 )  ret=NULL; /* si pas P5 alore ERREUR */
	else{
		fgets(ch,255,f); /* la taille */
		while(ch[0]=='#')  fgets(ch,255,f); /* lecture éventuelle des commentaires */
		sscanf(ch,"%d %d",w,h);
		fgets(ch,255,f); /* le fameux 255 */
		s = (*w)*(*h);
		ret = (unsigned char*) malloc(s*sizeof(unsigned char));
		if( (c!=EOF) && (ret!=NULL)) { /* 255 bien lu et l'allocation s'est bien passée */
			i=0;
			while( (i<s) && (c!=EOF)) {
				ret[i++]=(unsigned char) (c = fgetc(f));
			}
			if( (i<s) || ((i==s) && (c==EOF)) ) {
				ret = NULL; /* erreur de lecture */
			}
		}
	}
	return ret;
}


/* --------------------------------------------------------------- 
 *  Function ecritPGM : 
 *
 *  @param FILE* : le fichier dans lequel on écrit (éventuellement stdout)
 *  @param unsigned char*: la matrice contenant l'image
 *  @param int : la largeur (nombre de colonnes)
 *  @param int : la hauteur (nombre de lignes)
 *  @return FILE* : NULL si erreur, le fichier dans lequel on a écrit.
 * --------------------------------------------------------------- */
FILE* ecritPGM(FILE* f, unsigned  char* img, int w, int h)
{
	int i		= 0; /* i compteur de boucle */
	int s		= 0; /* s taille de l'image  */
	FILE* ret	= f;
	int erreur	= 0;
		
	erreur=fputs("P5\n",f);	/* le format */
	if( erreur == EOF ){	/* si erreur d'ecriture */
		fclose(f);
		ret = NULL;
	} else {
		erreur=fprintf(f,"%d %d\n",w,h);
		erreur = fprintf(f,"255\n");	/* le fameux 255 */
		if(erreur!=4){
			fclose(f);
			ret = NULL;
		} else {
			s = w*h;
			i=0;
			while( (i<s) && (erreur!=EOF)){
				erreur=fputc((int)img[i++],f);
			}
			if( (i<s) || ((i==s) && (erreur==EOF)) ) {
				ret = NULL; /* erreur de lecture */
			}
		}
	}
	return ret;  
}

void chamferIterative(image2D* in, image2D* out, mask2D* mask, unsigned char maxDist) {
	// allocate a stack : 
	stack s;
	initStack(&s);

	// allocate the out image, and the center of the image :
	(*out).pixels = (unsigned char**) malloc((*in).h * sizeof(unsigned char*));
	for (int i = 0; i < (*in).h; ++i) {
		(*out).pixels[i] = (unsigned char*) malloc((*in).w*sizeof(unsigned char));
	}

	(*out).h = (*in).h;
	(*out).w = (*in).w;

	for (int i = 0; i < (*out).h; ++i) {
		for (int j = 0; j < (*out).w; ++j) {
			(*out).pixels[i][j] = 0;
		}
	}

	int imgCenterX = ((*in).w % 2 == 1) ? (((*in).w)-1)/2 : ((*in).w)/2;
	int imgCenterY = ((*in).h % 2 == 1) ? (((*in).h)-1)/2 : ((*in).h)/2;

	// int imgCenterX = ((*in).w)/2;
	// int imgCenterY = ((*in).h)/2;

	push(&s, imgCenterX-1, imgCenterY-1);

	while (isStackEmpty(&s) == 0) {
		// apply the kernel to the given point P :
		int* P = pop(&s);

		if (P[0] != -1) {
			// loop for the octant of the mask :
			int px = P[0];
			int py = P[1];
			// compute maskCenter
			int maskCenterX = ((*mask).w-1)/2;
			int maskCenterY = ((*mask).w-1)/2;
			int maskMaxX = (*mask).w;
			// if we're still below the maxDist boundary :
			if ((*out).pixels[py][px] <= maxDist) {
				for (int x = 1; x <= maskCenterX && px+x < (*in).w; ++x) {
					for (int y = 0; y <= x && py+y < (*in).h; ++y) {
						unsigned char maskVal = (*mask).values[maskCenterY + y][maskCenterX + x];
						unsigned char pixelOriginVal = (*out).pixels[py][px];
						unsigned char pixelTargetVal = (*out).pixels[py+y][px+x];
						if (maskVal != -1) {
							// if the value of center + mask < distance AND
							// if the value of center + mask < target point value
							if( pixelOriginVal + maskVal <= maxDist && ( pixelOriginVal + maskVal < pixelTargetVal || pixelTargetVal == 0) ) {
								// if the image has not been written yet, or it has 
								(*out).pixels[py+y][px+x] = pixelOriginVal + maskVal;
								if (isInStack(&s, px+x, py+y) == 0) {
									push(&s, px+x, py+y);
								}
							}
						}
					}
				}
			}
		} else {
			printf("chamferIterative() : CRITICAL ERROR : no more pixels in stack. %d\n", (*in).w);
		}
	}

	#ifdef MAX_OUTPUT
	float coef = 1.0 / ((float)maxDist);
	for (int y = 0; y < (*in).h; ++y) {
		for (int x = 0; x < (*in).w; ++x) {
			(*out).pixels[y][x] = ( (*out).pixels[y][x] > 0 ) ? (unsigned char) ((255.0 * (float)(*out).pixels[y][x])*coef) : 0;
		}
	}
	#endif

	#ifdef DEBUG
	printf("finished while loop\n");
	#endif
}

image2D copyImage(image2D* img) {
	image2D n_img;

	n_img.w = img->w;
	n_img.h = img->h;

	n_img.pixels = (unsigned char**)malloc(n_img.h * sizeof(unsigned char*));
	for (int i = 0; i < n_img.h; ++i) {
		n_img.pixels[i] = (unsigned char*)malloc(n_img.w * sizeof(char));
	}

	for (int i = 0; i < img->h; ++i) {
		for (int j = 0; j < img->w; ++j) {
			n_img.pixels[i][j] = img->pixels[i][j];
		}
	}

	return n_img;
}

int min(int a, int b) {
	return (a < b) ? a : b;
}

void distanceImage(image2D* in, image2D* out, mask2D* m) {
	// lol
	// firstpass
	#ifdef ANIMATE_RESULTS
	char fn[20] = "animation/p1_000.pgm";
	FILE* frame = fopen(fn, "w");
	#endif

	// base immage out
	image2D out2 = copyImage(out);
	
	for (int i = 0; i < in->w; ++i) {
	
		#ifdef ANIMATE_RESULTS
		if (i % 2 == 0 && frame != NULL) {
			frame = ecritPGM(frame, convertTo1D(*out), in->w, in->h);
		}
		#endif
	
		for (int j = 0; j < in->w; ++j) {
			if (in->pixels[i][j] != 0) {
				/**
				 * et là lol la complexité fait comme little boy à hiroshima,
				 * elle explose
				 */
				int maskC = m->w/2;
				for (int x = 0; x <= maskC; ++x) {
				if (i+x < in->w) {
					for (int y = 0; y <= maskC; ++y) {
					if (j+y < in->w && m->values[x][y] != -1) {
						int newval = out->pixels[i][j] + (unsigned char)floor((double)m->values[maskC+x][maskC+y] / (double)m->values[maskC][maskC+1]);
						int curval = out->pixels[i+x][j+y];
						if (newval < curval || curval == 0) {
							out->pixels[i+x][j+y] = newval;
						}
					}
					}
				}
				}
			} else {
				out->pixels[i][j] = 0;
			}
		}
	
	
		#ifdef ANIMATE_RESULTS
		if (i%2 == 0 && frame == NULL) {
			printf("Error printing frame %i of phase 1\n", i);
		}
		if (i % 2 == 0) {
			fn[13] = (char) 48 + i / 100;
			fn[14] = (char) 48 + (i/10)%10;
			fn[15] = (char) 48 + i % 10;
			fclose(frame);
			frame = fopen(fn, "w");
		}
		#endif
	}
	
	
	#ifdef ANIMATE_RESULTS
	fn[11] = 50;
	fclose(frame);
	char fn2[20] = "animation/p2_000.pgm";
	frame = fopen(fn2, "w");
	#endif


	for (int i = in->w-1; i >= 0; --i) {
		
		#ifdef ANIMATE_RESULTS
		if (i%2 == 0 && frame != NULL) {
			frame = ecritPGM(frame, convertTo1D(*out), in->w, in->h);
		}
		#endif
		
		for (int j = in->w-1; j >= 0; --j) {
			if (in->pixels[i][j] != 0) {
				/**
				 * et là lol la complexité fait comme little boy à hiroshima,
				 * elle explose
				 */
				int maskC = m->w/2;
				for (int x = 0; x <= maskC; ++x) {
				if (i-x >= 0) {
					for (int y = 0; y <= maskC; ++y) {
					if (j-y >= 0 && m->values[x][y] != -1) {
						int newval = out2.pixels[i][j] + (unsigned char)floor((double)m->values[maskC-x][maskC-y] / (double)m->values[maskC][maskC+1]);
						int curval = out2.pixels[i-x][j-y];
						if (newval < curval || curval == 0) {
							out2.pixels[i-x][j-y] = (unsigned char)newval;
						}
					}
					}
				}
				}
			}
		}

		#ifdef ANIMATE_RESULTS
		if (i%2 == 0 && frame == NULL) {
			printf("Error printing frame %i of phase 1\n", i);
		}
		if (i%2 == 0) {
			fn2[13] = (char) 48 + i / 100;
			fn2[14] = (char) 48 + (i/10)%10;
			fn2[15] = (char) 48 + i % 10;
			fclose(frame);
			frame = fopen(fn2, "w");
		}
		#endif
	}
	#ifdef ANIMATE_RESULTS
	fclose(frame);
	#endif

	for (int i = 0; i < in->w; ++i) {
		for (int j = 0; j < in->w; ++j) {
			out->pixels[i][j] = (unsigned char) min((int)out->pixels[i][j], (int)out2.pixels[i][j]);
		}
	}

	return;
}

#endif
