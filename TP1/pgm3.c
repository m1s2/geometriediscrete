/* This may look like C code, but it is really -*- C -*-
 
	 Copyright (C) 1998 LIRMM
 
	 Author: Christophe Fiorio <fiorio@lirmm.fr>
	 This is free software; you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation; either version 2, or (at your option)
	 any later version.
 
	 This sofware is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.
 
	 You may have received a copy of the GNU General Public License along
	 with GNU Emacs; see the file COPYING.  If not, write to the Free
	 Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 
	@(#) $Id: pgm2.c,v 1.1 2000/03/21 19:12:20 fiorio Exp $
 
	$Log: pgm2.c,v $
	Revision 1.1  2000/03/21 19:12:20  fiorio
	Initial revision


*/

#ifndef pgm2_c
#define pgm2_c

#include "pgm_basic_io.h"
#include <stdlib.h>

int main(int argc, const char* argv[])
{
	int ret			= -1;
	unsigned char* img	= NULL;
	image2D img2D;
	image2D whiteCanvas;
	mask2D newmask;
	FILE* fichierIn		= NULL;
	FILE* fichierOut	= NULL;
	int w			= 0;
	int h			= 0;
	
	/**
	 * Checks the number of arguments
	 */
	if(argc != 3){
		printf("ERREUR -- usage :\n");
		printf("%s file_name_in file_name_out \n",argv[0]);
		return ret;
	}
	/**
	 * Read files
	 */
	else {
		fichierIn = fopen(argv[1],"r");
		fichierOut= fopen(argv[2],"w");
		if(fichierIn != NULL && fichierOut != NULL){

			/**
			 * Convert it to a 2D matrix
			 */
			int sizeW;
			int sizeH;
			img = litPGM(fichierIn, &sizeW, &sizeH);
			img2D = convertTo2D(img, sizeW, sizeH);
			whiteCanvas = createImage(sizeW, sizeH);

			newmask.w = 3;
			newmask.values = ( char**)malloc(newmask.w * sizeof(unsigned char *));
			for (int i = 0; i < newmask.w; ++i) {
				newmask.values[i] = ( char*)malloc(newmask.w * sizeof(char));
			}

			newmask.values[0][0] = 4;
			newmask.values[0][1] = 3;
			newmask.values[0][2] = 4;
			// newmask.values[0][3] = 11;
			// newmask.values[0][4] = -1;

			newmask.values[1][0] = 3;
			newmask.values[1][1] = 0;
			newmask.values[1][2] = 3;
			// newmask.values[1][3] =  7;
			// newmask.values[1][4] = 11;

			newmask.values[2][0] = 4;
			newmask.values[2][1] = 3;
			newmask.values[2][2] = 4;
			// newmask.values[2][3] =  5;
			// newmask.values[2][4] = -1;

			// newmask.values[3][0] = 11;
			// newmask.values[3][1] =  7;
			// newmask.values[3][2] =  5;
			// newmask.values[3][3] =  7;
			// newmask.values[3][4] = 11;

			// newmask.values[4][0] = -1;
			// newmask.values[4][1] = 11;
			// newmask.values[4][2] = -1;
			// newmask.values[4][3] = 11;
			// newmask.values[4][4] = -1;

			distanceImage(&img2D, &whiteCanvas, &newmask);
			free(img);
			img = convertTo1D(whiteCanvas);

			if( (fichierOut=ecritPGM(fichierOut,img,sizeW,sizeH)) != NULL ) {
				ret = 0;
			}
		} else {
			printf("error opening file %s\n", argv[1]);
			printf("error opening file %s\n", argv[2]);
			return ret;
		}
	}
	return ret;
}


#endif
