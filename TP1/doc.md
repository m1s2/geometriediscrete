# HMIN235B - TP boules de chamfrein

### Réponses aux questions de TD :

1. Le premier masque correspond à la distance de Manhattan (si on ignore les cases contenant un tiret, sinon il représente la distance euclidienne); alors que le deuxième correspond à la distance de l'échiqiuer.

2a. La propriété triangulaire permet d'avoir des masques incomplets mais valides.

2b. Pour le premier masque, on peut approximer la distance représentée par des tirets par 2. Pour le deuxième masque, la distance aux coins est de 14, tandis que la distance sur les milieux des côtés est de 10.

3. Regardez le code pour voir l'algorithme ! 

4. Pour le premier masque, $a = 8$ et $b = 11$ correspondent le mieux à un masque qui approxime la distance euclidienne, l'erreur que présente ce masque est de $\delta(e) = abs(\frac{11}{8} - \sqrt{2} = 0.03921$. Pour le second, il ne semble pas qu'il y aie trop de possibilités ! Son erreur relative est de $\delta(e) = \text{abs}(\frac{11}{5} - \sqrt{3}) = 0.4679$. Et finalement pour le dernier, c'est pour les valeurs $a = 19$, $b = 27$, $c = 42$, $d = 60$, et $e = 68$. Son erreur relative est de $\approx 2.96$.

5. Il faut que $c < a+b$, sinon l'inégalité triangulaire n'est pas respectée.

### Doc du programme : 

Vous pouvez compiler le programme en utilisant `make`. Le programme compilé se nommera `tp1`, et il prends en argument un nom de fichier de sortie, une taille d'image de sortie, et un rayon pour la boule (infèrieur à 255, dû aux limitations du format `unsigned char`.

Attention, le programme ne vérifie pas que vous lui passiez des arguments valides, donc si vous entrez un mot dans le champ de taille, vous entrez dans un cas de comportement non défini.

Actuellement, le programme contient le masque de taille 5x5, avec comme coefficients 5,7, et 11 (comme vu dans le TD).

### À propos des boules de chamfrein demandées

L'algorithme, dans son état actuel, ne supporte que les masques réguliers, car il ne calcule que les distances dans un huitième de l'image, avant d'effectuer une symétrie en 8 pour le reste de la boule. J'ai donc pu répondre aux questions du TP pour les boules de chamfrein qui sont issues d'un masque régulier (donc pas celles issues des masques des questions 5 et 8). Les boules générées par les masques qui ont pu être appliqués sont contenues dans le dossier `question_1_results`. Voici donc les masques utilisés, par question : 

1. $$\begin{vmatrix} - & 1 & - \\ 1 & 0 & 1 \\ - & 1 & - \end{vmatrix}$$
2. $$\begin{vmatrix} 1 & 1 & 1 \\ 1 & 0 & 1 \\ 1 & 1 & 1 \end{vmatrix}$$
3. $$\begin{vmatrix} 4 & 3 & 4 \\ 3 & 0 & 3 \\ 4 & 3 & 4 \end{vmatrix}$$
4. $$\begin{vmatrix} - & 11 & - & 11 & - \\ 11 & 7 & 5 & 7 & 11 \\ - & 5 & 0 & 5 & - \\ 11 & 7 & 5 & 7 & 11 \\ - & 11 & - & 11 & - \end{vmatrix}$$
5. $$\text{Non possible grâce à cet algo}$$
6. $$\begin{vmatrix} - & 9 & - & 9 & - \\ 9 & 7 & 5 & 7 & 9 \\ - & 5 & 0 & 5 & - \\ 9 & 7 & 5 & 7 & 9 \\ - & 9 & - & 9 & - \end{vmatrix}$$
7. $$\begin{vmatrix} 1 & 3 & 1 \\ 3 & 0 & 3 \\ 1 & 3 & 1 \end{vmatrix}$$
8. $$\text{Non possible grâce à cet algo}$$

