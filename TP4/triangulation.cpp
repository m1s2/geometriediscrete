#include "triangulation.hpp"

#include <stack>
#include <utility>
#include <algorithm>
#include <iostream>

triangulationStructures::halfEdge::halfEdge(unsigned int _s, unsigned int _e) {
	this->startVertex = _s;
	this->endVertex = _e;
	this->next = nullptr;
	this->previous = nullptr;
	this->opposite = nullptr;
}

triangulationStructures::triangulation::triangulation(std::vector<Point2> _pc) {
	this->pointCloud = _pc;
	this->combinatorialMap = nullptr;
}

bool triangulationStructures::triangulation::operator()(triangulationTypes _type) {
	if (_type == triangulationStructures::triangulationTypes::DELAUNAY) {
		return this->constructDelaunay();
	} else {
		return this->triangulate();
	}
}

void halfEdge::weaveForward(halfEdge* _h) {
	this->next = _h;
	_h->previous = this;
}

void halfEdge::weaveBackward(halfEdge* _h) {
	_h->next = this;
	this->previous = _h;
}

void triangulationStructures::halfEdge::addNeighbors(std::set<halfEdge*>& nb) {
	// see if the edge and its opposite have been added already : 
	if (nb.find(this) != nb.end() && nb.find(this->opposite) != nb.end()) {
		return;
	}
	// if not, we have some adding to do :
	auto result = nb.insert(this);
	// auto should be pair<iter, bool> for ::insert()
	result = nb.insert(this->previous);
	result = nb.insert(this->next);
	this->opposite->addNeighbors(nb);
	this->previous->opposite->addNeighbors(nb);
	this->next->opposite->addNeighbors(nb);
	return;
}

halfEdge* triangulationStructures::getOutsideOfTriangle(halfEdge* startPoint) {
	// if the edge is already outside, return it
	if (!isInsideOfTriangle(startPoint)) { return startPoint; }

	// if not, watch the rest of the edges
	std::set<halfEdge*> allEdges = std::set<halfEdge*>();
	startPoint->addNeighbors(allEdges);
	std::cout << "visiting " << allEdges.size() << " edges" << std::endl;
	for (auto e = allEdges.begin(); e != allEdges.end(); ++e) {
		if (!isInsideOfTriangle(*e)) {
			return *e;
		}
	}
	// std::cout << "Your next line will be : \nSegmentation Fault (core dumped)" << std::endl;
	std::cout << "none found : " << std::endl;
	for (auto e = allEdges.begin(); e != allEdges.end(); ++e) {
		std::cout << *(*e) << std::endl;
	}
	return nullptr;
}

halfEdge* triangulationStructures::createEdge(unsigned int _e, unsigned int _n) {
	/**
	 * _e : existing vertex which is already connected
	 * _n : new vertex to insert into the triangulation
	 * _h : halfEdge which ends at the point _e
	 */

	halfEdge* b1 = new halfEdge(_e,_n);
	halfEdge* b2 = new halfEdge(_n,_e);
	weave2(b1,b2);
	return b1;
}

void triangulationStructures::insertEdge(unsigned int _e, unsigned int _n, halfEdge* _h) {
	/**
	 * _e : existing vertex which is already connected
	 * _n : new vertex to insert into the triangulation
	 * _h : halfEdge which ends at the point _e
	 */

	// Half-edge going TO new point
	halfEdge* newEdge = createEdge(_e,_n);
	weave1(_h, newEdge);
}

halfEdge* triangulationStructures::createTriangle(unsigned int _es, unsigned int _ee, unsigned int _n, halfEdge* _h) {
	/**
	 * _es : existing vertex, start
	 * _ee : existing vertex, end
	 * _n  : new vertex [[to add]]
	 * _h  : [[non-]]existing halfedge. if not, creates new triangle
	 */
	halfEdge* baseEdge;
	// if the edge ain't created, do so
	if (_h == nullptr) {
		// create the edge we need to "solder" the other ones on
		baseEdge = createEdge(_es, _ee);
		// create the half edges to put on
		halfEdge* e1 = createEdge(_ee, _n);
		halfEdge* e2 = createEdge(_n, _es);
		weave1(e1,e2);
		weave1(e2, baseEdge);
		weave1(baseEdge, e1);
		return baseEdge;
	} else {
		baseEdge = _h;
		// if it was already created, 
		halfEdge* e1 = createEdge(_es, _n);
		halfEdge* e2 = createEdge(_n, _ee);
		if (_h->startVertex != _es || _h->endVertex != _ee) {
			baseEdge = _h->opposite;
			weave1(e1,e2);
			weave1(baseEdge,e2->opposite);
			weave1(baseEdge->previous,e1);
		} else {
			weave1(e1,e2);
			weave1(baseEdge,e2->opposite);
			weave1(baseEdge->previous,e1);
		}
	}
	return nullptr;
}

void triangulationStructures::weave2(halfEdge* b1, halfEdge* b2) {
	b1->next = b2;
	b1->opposite = b2;
	b1->previous = b2;

	b2->next = b1;
	b2->opposite = b1;
	b2->previous = b1;
}

void triangulationStructures::weave1(halfEdge* b1, halfEdge* b2) {
	// keep a tab on the (previous) next edge
	halfEdge* next = b1->next;
	b1->next = b2;
	b2->previous = b1;
	b2->opposite->next = next;
	next->previous = b2->opposite;
}

bool triangulationStructures::isInsideOfTriangle(halfEdge* _he) {
	return _he->next->next->next == _he;
}

bool triangulationStructures::triangulation::isEdgeVisible(halfEdge* _he, unsigned int _c, const Point2& _center) {
	// Vector from _c to the START of _he (hence the _c + _he + Start)
	vec2 _c_he_s = vec2(this->pointCloud[_he->startVertex] - this->pointCloud[_c]);
	vec2 _c_he_e = vec2(this->pointCloud[_he->endVertex] - this->pointCloud[_c]);
	// Vector from the center to the START of _he (hence the _center + _he + Start)
	vec2 _center_he_s = vec2(this->pointCloud[_he->startVertex] - _center);
	vec2 _center_he_e = vec2(this->pointCloud[_he->endVertex] - _center);
	// check the side of end compared to their base vectors using determinant :
	bool cSide = (_c_he_s.x * _c_he_e.y - _c_he_s.y * _c_he_e.x > 0.0) ? true : false;
	// check same for center :
	bool centerSide = (_center_he_s.x * _center_he_e.y - _center_he_s.y * _center_he_e.x > 0.0) ? true : false;
	// for the next part, it doesn't really matter if we analysed the vectors 
	// for concavity or convexity, we just need their result to be on either side of 0
	// std::cout << "edge was " << ((cSide xor centerSide) ? "visible" : "not visible") << std::endl;
	return cSide xor centerSide;
}

bool triangulationStructures::triangulation::isEdgeLegal(halfEdge* _he) {
	// we have to check first if the edge is the boundary
	// between two triangles, because if it's not, then
	// this edge will always be legal :
	if (isInsideOfTriangle(_he) && isInsideOfTriangle(_he->opposite)) {
		// we first get the center of both circumscribed circles
		// to the two triangles :
		Point2 center1 = getCenterOfCircumscribedCircle(_he, this->pointCloud);
		Point2 center2 = getCenterOfCircumscribedCircle(_he->opposite, this->pointCloud);
		double r1 = length(vec2(this->pointCloud[_he->endVertex] - center1));
		double r2 = length(vec2(this->pointCloud[_he->opposite->endVertex] - center2));
		if (length(vec2(this->pointCloud[_he->endVertex] - center2)) < r1) {
			return false;
		}
		if (length(vec2(this->pointCloud[_he->opposite->endVertex] - center1)) < r2) {
			return false;
		}
		return true;
	} else {
		return true;
	}
	
}

Point2 triangulationStructures::getCenterOfCircumscribedCircle(halfEdge* _he, const std::vector<Point2> pointCloud) {
	/**
	 * Warning : this function makes the big assumption a triangle
	 * was passed in arg. It will do no check if the halfEdge passed
	 * is actually in a triangle or not.
	 * 
	 * Make of it what you will.
	 */
	// get two edges, make them into vectors :
	vec2 edge1 = vec2(pointCloud[_he->endVertex] - pointCloud[_he->startVertex]);
	vec2 edge2 = vec2(pointCloud[_he->next->endVertex] - pointCloud[_he->endVertex]);
	// get the bisectors of the edges, by rotating them 90° around the point
	// in the middle of the vector :
	Point2 middle1 = Point2( (pointCloud[_he->endVertex] - pointCloud[_he->startVertex]) / 2.0);
	Point2 middle2 = Point2( (pointCloud[_he->next->endVertex] - pointCloud[_he->endVertex])  / 2.0);
	vec2 mediat1 = vec2(edge1.x * std::cos(DegreesToRadians*90.0) - edge1.y * std::sin(DegreesToRadians*90.0));
	vec2 mediat2 = vec2(edge2.x * std::cos(DegreesToRadians*90.0) - edge2.y * std::sin(DegreesToRadians*90.0));
	// compute slopes of bissectors :
	double m1 = (mediat1.y) / (mediat1.x);
	double m2 = (mediat2.y) / (mediat2.x);
	double b1 = middle1.y - m1 * middle1.x;
	double b2 = middle2.y - m2 * middle2.x;
	double px = (b2-b1) / (m1-m2);
	double py = m1 * px + b1;
	return Point2(px, py);
}

void triangulationStructures::triangulation::flipEdge(halfEdge* _he) {
	if (isInsideOfTriangle(_he) && isInsideOfTriangle(_he->opposite)) {
		// we get the new indices for the edge to construct :
		unsigned int _newedge_start = _he->next->endVertex;
		unsigned int _newedge_end = _he->opposite->next->endVertex;
		// we get pointers to the edges to weave together :
		halfEdge* _insertionpoint_begin = _he->next;
		halfEdge* _insertionpoint_end = _he->opposite->next;
		// we remove the edge from the model :
		_he->previous->next = _he->opposite->next;
		_he->opposite->previous->next = _he->next;
		// we create the nex edge :
		halfEdge* _newedge = createEdge(_newedge_start, _newedge_end);
		// weave it with the corresponding edges :
		weave1(_insertionpoint_begin, _newedge);
		weave1(_insertionpoint_end, _newedge->opposite);
		// We now succesfully have swapped an edge
	} else {
		std::cout << "Could not flip edge " << _he << " because it was not the boundary between two triangles" << std::endl;
		return;
	}
}

bool triangulationStructures::triangulation::triangulate() {
	std::cerr << "You are currently triangulating using a visibility algorithm." << std::endl;

	/**
	 * start by sorting the vertices
	 */
	std::sort(this->pointCloud.begin(), this->pointCloud.end(), [](const Point2 lVal, const Point2 rVal){
		if (lVal.x == rVal.x) {
			return lVal.y < rVal.y;
		}
		return lVal.x < rVal.x;
	});

	/**
	 * Once sorted, create a first draft of a combinatorial map
	 */
	this->combinatorialMap = createTriangle(0,1,2,nullptr);

	Point2 centroid( (this->pointCloud[0] + this->pointCloud[1] + this->pointCloud[2]) / 3.0 );
	halfEdge* latestEdge = nullptr;
	// pointers for going through the boundary edge, and for the lower and higher bound of the visible edges
	std::cout << "Starting loop : at vertex ";
	/**
	 * For all remaining points in the cloud, do the following :
	 */
	for (unsigned int i = 3; i < this->pointCloud.size(); ++i) {
		// helpful pointers to go through the boundary
		halfEdge *parkour = nullptr, *rparkour = nullptr, *lower = nullptr, *higher = nullptr; 
		// if it's the first iteration, set the latest edge as the first one
		if (i == 3) latestEdge = this->combinatorialMap;
		// if we're inside a triangle, get out of it.
		parkour = latestEdge;
		// get outside of triangle :
		if (i>3 && isInsideOfTriangle(parkour)) {
			parkour = getOutsideOfTriangle(parkour);
			if (parkour == nullptr) {
				std::cout << "We could not find any outside edge for vertex " << i << std::endl;
				return false;
			}
		}
		// initialise exploration of the boundary
		halfEdge* stoppingCriteria = rparkour = parkour;
		/**
		 * Check, from the latest edge added, into of the boundary edges
		 * of the current non-complete triangulation << T' >>, which ones
		 * are visible relative to the point.
		 */
		do {
			if (lower == nullptr && higher == nullptr && isEdgeVisible(parkour, i, centroid)) {
				lower = higher = parkour;
			} else {
				if (isEdgeVisible(parkour, i, centroid) && parkour->previous == higher) {
					higher = parkour;
				}
				if (isEdgeVisible(rparkour, i, centroid) && rparkour->next == lower) {
					lower = rparkour;
				}
			}
			parkour = parkour->next;
			rparkour = rparkour->previous;
		} while (parkour != stoppingCriteria);
		if (higher == nullptr) {
			std::cout << "NONE FOUND" << std::endl;
			return false;
		}
		if (lower == higher) {
			// if there is only one edge visible, create a triangle
			halfEdge* previousEdge = lower->previous;
			halfEdge* upToNewPoint = createEdge(lower->startVertex, i);
			halfEdge* downToExistingPoint = createEdge(lower->endVertex, i);
			weave1(upToNewPoint, downToExistingPoint->opposite);
			weave1(previousEdge, upToNewPoint);
			weave1(lower, downToExistingPoint);
			latestEdge = upToNewPoint;
		} else {
			// get some helpful pointers
			halfEdge* previousEdge = lower->previous;
			halfEdge* nextEdge = lower->next;
			// we now have the two edges (last and first) of the arc to fuse together :
			halfEdge* begginingEdge = createEdge(lower->startVertex, i);
			// we update the latest edge created for the next iteration
			latestEdge = begginingEdge;
			// we weave the edges together
			weave1(previousEdge, begginingEdge);
			#ifdef UNSAFE_LINKING_EDGES
			do {
				halfEdge* lowerBackup = lower; 
				lower = lower->next;
				begginingEdge = createEdge(lower->startVertex, i);
				weave1(lowerBackup, begginingEdge);
				weave1(previous, begginingEdge->opposite);
			} while (lower->previous != higher);
			halfEdge* lastEdge = createEdge(higher->endVertex, i);
			weave1(higher, lastEdge);
			weave1(previous, lastEdge->opposite);
			#else
			do {
				halfEdge* endingEdge = createEdge(lower->endVertex, i);
				weave1(lower, endingEdge);
				weave1(begginingEdge, endingEdge->opposite);
				previousEdge = lower;
				lower = nextEdge;
				nextEdge = lower->next;
			} while (lower != higher);
			halfEdge* lastEdge = createEdge(higher->endVertex, i);
			weave1(higher, lastEdge);
			weave1(begginingEdge, lastEdge->opposite);
			#endif
		}
		// update centroid position
		centroid = centroid + ( Point2(this->pointCloud[latestEdge->startVertex] + this->pointCloud[latestEdge->endVertex] + this->pointCloud[higher->endVertex]) / 3.0);
		centroid /= 2.0;
		lower = nullptr;
		higher = nullptr;
		// this->toPly("result.ply");
		// sleep(5);
		std::cout << i << ", "; 
	}
	std::cout << " .. ended loops" << std::endl;
	return true;
}

bool triangulationStructures::triangulation::constructDelaunay() {
	return false;
}

void triangulationStructures::triangulation::toPly(const std::string& pathString) const {
	std::ofstream out(pathString);

	std::cout << "Starting recursion" << std::endl;
	std::set<halfEdge*> allEdges = std::set<halfEdge*>();
	this->combinatorialMap->addNeighbors(allEdges);
	std::cout << "ending recursion" << std::endl;

	out << "ply" << std::endl;
	out << "format ascii 1.0" << std::endl;
	out << "element vertex " << this->pointCloud.size() << std::endl;
	out << "property float x" << std::endl;
	out << "property float y" << std::endl;
	out << "property float z" << std::endl;
	if (this->combinatorialMap != nullptr) {
		out << "element edge " << allEdges.size() << std::endl;
		out << "property int vertex1" << std::endl;
		out << "property int vertex2" << std::endl;
	}
	out << "end_header" << std::endl;
	for (auto p = this->pointCloud.begin(); p != this->pointCloud.end(); ++p) {
		out << p->x << " " << p->y << " 0.0" << std::endl;
	}
	for (auto e = allEdges.begin(); e != allEdges.end(); ++e) {
		out << (*e)->startVertex << " " << (*e)->endVertex << std::endl;
	}
	out << std::endl;
	out.close();
	return;
}

std::vector<Point2> randomizedPoints(const Point2 center, double radius, unsigned int number) {
	std::vector<Point2> generatedPoints;

	// Seed from current time
	std::srand(std::time(nullptr));

	for (unsigned int i = 0; i < number; ++i) {
		// random distance in [0,radius]
		double distance = radius * ( (double)rand() / ((double)RAND_MAX+1) );
		// random angle
		double angle = 2.0 * M_PI * ( (double)rand() / ((double)RAND_MAX+1) );
		// position on the unit circle
		Point2 unitCirclePos = Point2(std::cos(angle), std::sin(angle));
		// scaled
		Point2 circlePos = unitCirclePos * distance;
		// translated
		Point2 finalPos = circlePos + center;
		// add to vector
		generatedPoints.push_back(finalPos);
	}

	return generatedPoints;
}
