/**
 * =================================================================
 * FILE : triangulation.hpp
 * DESC : Structs for discrete geometry in {2/3}D, related to trian-
 *        -gulation algorithms.
 * AUTH : Thibault de VILLÈLE
 * DATE : 24th April 2019
 * =================================================================
 */

#ifndef TRIANGULATION_HPP
#define TRIANGULATION_HPP

#include "Vector.hpp"

#include <cstdlib>
#include <limits>
#include <vector>
#include <ctime>
#include <unistd.h>
#include <set>

/**
 * Macro to indicate there is no 
 * triangle assigned to that edge yet
 */
#define NO_TRIANGLE std::numeric_limits<unsigned int>::max()

/**
 * Macro to indicate there exists no point
 * being the center of the circumscribed circle
 * to a triangle (usually when there is no triangle)
 */
#define NO_CENTER Point2(std::numeric_limits<double>::max(), std::numeric_limits<double>::max())

// #define UNSAFE_LINKING_EDGES

namespace triangulationStructures {
	/**
	 * An edge structure, which is composed of two half-edges
	 * intertwined with each other. Two indexes of vertices
	 * allow to know which vertices they connect, and then
	 * we'll have three pointers : one for the **next** edge,
	 * one for the **previous** edge, and one for the **opposite**
	 * edge.
	 */
	struct halfEdge {
		unsigned int startVertex;
		unsigned int endVertex;
		halfEdge* next;
		halfEdge* previous;
		halfEdge* opposite;

		/**
		 * Basic constructor. Will build a half-edge without any pointers,
		 * which will be added later be the function `weave2()`.
		 */
		halfEdge(unsigned int, unsigned int);
	
		/**
		 * Weaves the current half-edge with another half-edge
		 */
		void weaveForward(halfEdge*);

		/**
		 * Weaves the current half-edge with another half-edge
		 */
		void weaveBackward(halfEdge*);

		/**
		 * recursively include all elements of the edges into a set
		 */
		void addNeighbors(std::set<halfEdge*>&);

		/**
		 * Extraction operator, for easy printing
		 */
		friend std::ostream& operator<<(std::ostream& out, const halfEdge& e) {
			out << &e << " - Relie " << e.startVertex << " et " << e.endVertex;
			out << ", suivant " << e.next << " opposé " << e.opposite << " et precedent " << e.previous << std::endl;
			return out;
		}
	};

	/**
	 * Creates a double-edge
	 */
	halfEdge* createEdge(unsigned int, unsigned int);

	/**
	 * Allows for the creation of two half-edges connecting to the points
	 * defined by S and E, from the end of the half-edge H.
	 */
	void insertEdge(unsigned int, unsigned int, halfEdge*);

	/**
	 * Creates a triangle between points A,B,C, with [[non-]]existing
	 * edge H. It will create [[3]]2 half-edges
	 */
	halfEdge* createTriangle(unsigned int, unsigned int, unsigned int, halfEdge*);

	/**
	 * Weaves two opposite half-edges
	 */
	void weave2(halfEdge*, halfEdge*);

	/**
	 * checks if a half-edge is the inside of a triangle or not
	 */
	bool isInsideOfTriangle(halfEdge*);

	/**
	 * returns an edge outside of a triangle, going from the edge specified
	 */
	halfEdge* getOutsideOfTriangle(halfEdge*);

	/**
	 * returns a point, which is the center of the circumscribed
	 * circle to the triangle for the halfEdge passed in arg
	 */
	Point2 getCenterOfCircumscribedCircle(halfEdge*, const std::vector<Point2>);

	/**
	 * weaves a half-edge at the end of the given half-edge
	 */
	void weave1(halfEdge*, halfEdge*);

	typedef enum {
		REGULAR,
		DELAUNAY
	} triangulationTypes;

	/**
	 * Structure for the triangulation
	 */
	struct triangulation {
		/**
		 * The point cloud to triangulate in 2D.
		 */
		std::vector<Point2> pointCloud;
		/**
		 * The first edge created of the combinatorial
		 * map created during both triangulation 
		 * algorithms.
		 */
		halfEdge* combinatorialMap;
		/**
		 * Number of edges created
		 */
		std::size_t numberOfEdges;

		/**
		 * Creates a triangulation structure, which can later be 
		 */
		triangulation(std::vector<Point2>);

		/**
		 * Check if an edge _he if visible from _c compared to _center
		 */
		bool isEdgeVisible(halfEdge* _he, unsigned int _c, const Point2& _center);

		/**
		 * Checks if an edge is considered "legal", relative to the
		 * constraints imposed by the Delaunay triangulation
		 */
		bool isEdgeLegal(halfEdge*);

		/**
		 * Flips an edge for delaunay triangulations.
		 */
		void flipEdge(halfEdge*);

		/**
		 * Constructs a delaunay triangulation. Returns true on completion,
		 * and false if an error has been encountered.
		 */
		bool constructDelaunay();

		/**
		 * Constructs a regular triangulation. Returns true once completed,
		 * and false if an error has been encountered.
		 */
		bool triangulate();

		/**
		 * Triangulate the point cloud. By default, tries to construct a Delaunay
		 * triangulation, unless specifically told something else.
		 */
		bool operator()(triangulationTypes t = triangulationTypes::DELAUNAY);

		/**
		 * outputs the triangulation to the file specified
		 */
		void toPly(const std::string&) const;

		/**
		 * prints edges to a file
		 */
		void printEdges(std::ofstream&) const;
	};
}

typedef triangulationStructures::halfEdge halfEdge;
typedef triangulationStructures::triangulation triangulation;
typedef triangulationStructures::triangulationTypes triangulationTypes;

std::vector<Point2> randomizedPoints(const Point2 center, double radius, unsigned int number);

#endif
