#include <iostream>

#include "Vector.hpp"
#include "triangulation.hpp"

#include <vector>
#include <cstring>

int main(int argc, char* argv[]) {
	/**
	 * Lazy argument parsing
	 */
	switch (argc) {
		case 2:
			if (strcmp(argv[1], "--usage") == 0) {
				std::cout << "Usage : " << std::endl;
				std::cout << "\t" << argv[0] << std::endl;
				return EXIT_SUCCESS;
			}
			break;
		default:
			break;
	};


	// Max x boundary
	double xMax = 20.0;
	// Max y boundary
	double yMax = 20.0;
	// Center point
	Point2 center(xMax/2.0, yMax/2.0);
	Point2 A(10.0, 15.0);
	Point2 B(5.0, 10.0);
	Point2 C(15.0, 15.0);
	#ifdef BASIC_TESTING
	std::vector<Point2>allPoints = std::vector<Point2>();
	allPoints.push_back(A);
	allPoints.push_back(B);
	allPoints.push_back(center);
	allPoints.push_back(C);
	halfEdge* originalTriangle = triangulationStructures::createTriangle(0,1,2,nullptr);
	triangulationStructures::createTriangle(0,1,3,originalTriangle);
	halfEdge* newEdge = triangulationStructures::createEdge(0,3);
	weave1(originalTriangle->opposite, newEdge);
	halfEdge* currentEdge = originalTriangle;
	do {
		std::cout << *currentEdge;
		currentEdge = currentEdge->next;
	} while (currentEdge != originalTriangle);
	std::cout << "================" << std::endl;
	currentEdge = originalTriangle->next->opposite;
	do {
		std::cout << *currentEdge;
		currentEdge = currentEdge->next;
	} while (currentEdge != originalTriangle->next->opposite);
	#else 
	std::vector<Point2> allPoints = randomizedPoints(center, xMax/2.0, atoi(argv[1]));
	triangulation t(allPoints);
	t(triangulationTypes::REGULAR);
	t.toPly("result.ply");
	#endif
	return EXIT_SUCCESS;
}