# A propos de ce TP

Bonjour monsieur,

Sur ce TP, ayant passé beaucoup plus de temps à faire la structure de données nécessaire, plutot que le TP en lui même, je vous rends donc cette version incomplète du TP.

La triangulation sur le critère de visibilité fonctionne (sauf un cas particulier parlé plus bas), et ensuite, j'ai implémenté (mais non testé) les fonctions suivantes :

- fonction de flip()
- fonction de légalité d'arête
- fonction permettant de trouver facilement un triangle adjacent (dû à ladite structure de données) [[Celle la à étée testée ! Sinon, je n'aurais rien pu faire de ce TP]]
- fonction qui permet de trouver un centre de cercle circonsrit à un triangle (utilisée dans la légalité d'arête)

Ces fonctions n'ont pas étées testées, mais elles devraient fonctionner, à un détail d'implémeentation près.

Le plus difficile dans vos TP est honnêtement les structures de données à mettre en place afin de les faire. La triangulation de Delaunay n'est pas très difficile à implémenter, mais étant donné de la deadline qui nous est imposée, je ne peux pas le faire à temps.

### A propos de la triangulation par visibilité

Celle ci fonctionne correctement, sauf dans un seul cas : lorsque la premier triangle est formé, si le sommet suivant, ou celui juste après sont visibles par **exactement** deux arêtes, le programme ne peut pas trouver d'arêtes en dehors de la triangulation incomplète T pour l'itération suivante. Je ne sais pas pourquoi uniquement sur les deux premiers sommets après la création du triangle originel, mais c'est surement parce que j'ai mal lié les arêtes entre elles !

### Comment compiler/utiliser :

Lancez la commande `make`. Vous pouvez ensuite exécuter le programme (qui calcule donc une triangulation par visibilité) avec la commande `tp4 <nombre de points à générer aléatoirement>`. Attention, si le deuxième argument n'est pas fourni, le programme fait une faute de segmentation. [ je n'ai pas mis en place de safeguards ]

Merci de votre compréhension !
